/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based
    on the listID that is passed to it from the given data in cards.json. Then pass control back
    to the code that called it by using a callback function.
*/

const cards = require('./data/cards_1.json')

function cardDetailsWithListId (cardId, cards, callback) {
  try {
    setTimeout(() => {
      if (cards.hasOwnProperty(cardId)) {
        callback(null, cards[cardId])
      } else {
        console.log('cardNot found')
      }
    }, 2000)
  } catch (err) {
    console.log(err.message)
  }
}

module.exports=cardDetailsWithListId